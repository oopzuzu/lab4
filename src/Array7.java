import java.util.Scanner;

public class Array7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        int sum = 0;
        for(int i=0; i<arr.length; i++){
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
            sum += arr[i];
        }
        System.out.print("arr = ");
        for(int i=0; i<arr.length; i++){
            System.out.print(arr[i] + " ");
        }
        // int min = arr[0];
        // for(int i=1; i<arr.length; i++){
            // if(min>arr[i]){
            //     min = arr[i];
        int index = 0;
        for(int i=1; i<arr.length; i++){
            if(arr[index]>arr[i]){
                index = i;
            }
        }
        // System.out.println("min = " + min);
        System.out.println();
        System.out.println("sum = " + sum);
        System.out.println("avg = "+(double)sum/3);
        System.out.println("min = " + arr[index]);
        // ถ้า + index ไปด้วย ก็จะรู้ตำแหน่ง
    }
}
